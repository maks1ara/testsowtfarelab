import pytest

from basetest.base_test import BaseTest

from pageobject.home_page import HomePage
from pageobject.guide_page import GuidePage
from pageobject.login_page import LoginPage
from pageobject.learn_page import LearnPage
from pageobject.lesson_page import LessonPage
from pageobject.profile_page import ProfilePage
from pageobject.account_page import AccountPage
from pageobject.courses_page import CoursesPage
from pageobject.course_about_page import CourseAboutPage
from pageobject.achievements_page import AchievementsPage
from pageobject.friends_search_page import FriendsSearchPage


@pytest.mark.usefixtures('init_page')
class TestCase(BaseTest):

    @pytest.fixture()
    def init_page(self, request):
        driver = request.cls.driver

        self.homePage = HomePage(driver)
        self.guidePage = GuidePage(driver)
        self.loginPage = LoginPage(driver)
        self.learnPage = LearnPage(driver)
        self.lessonPage = LessonPage(driver)
        self.profilePage = ProfilePage(driver)
        self.accountPage = AccountPage(driver)
        self.coursesPage = CoursesPage(driver)
        self.courseAboutPage = CourseAboutPage(driver)
        self.achievementsPage = AchievementsPage(driver)
        self.friendsSearchPage = FriendsSearchPage(driver)

    def registration(self):
        self.homePage.open_login()
        self.loginPage.open_learn()
        self.learnPage.skip_notification()

    def test_11(self):
        url = "https://www.duolingo.com/settings/account"

        self.registration()
        self.learnPage.open_profile()
        self.profilePage.open_account()
        assert self.accountPage.edit_profile() == url

    def test_12(self):
        title_text = "Все достижения"

        self.registration()
        self.learnPage.open_profile()
        self.profilePage.open_achievements()
        assert self.achievementsPage.check_achievement() == title_text

    def test_13(self):
        error_text = "К сожалению, мы не нашли пользователей по вашему запросу. Попробуйте поискать заново."

        self.registration()
        self.learnPage.open_profile()
        self.profilePage.open_friends_search()
        assert self.friendsSearchPage.find_friends() == error_text

    def test_14(self):
        error_text = "Неправильный адрес"

        self.registration()
        self.learnPage.open_profile()
        self.profilePage.open_account()
        assert self.accountPage.change_email() == error_text

    def test_21(self):
        title_text = "Раздел 1"

        self.registration()
        self.learnPage.open_courses()
        self.coursesPage.open_course_about()
        assert self.courseAboutPage.add_language() == title_text

    def test_22(self):
        title_text = "Справочник раздела 1"

        self.registration()
        self.learnPage.open_guide()
        assert self.guidePage.check_guide() == title_text

    def test_23(self):
        notification_text_03 = "Пройдите все уровни выше, чтобы открыть доступ!"

        self.registration()
        assert self.learnPage.open_block_lesson() == notification_text_03

    def test_31(self):
        color_wrong_answer = "rgba(255, 223, 224, 1)"

        self.registration()
        self.learnPage.open_lesson()
        assert self.lessonPage.skip_question() == color_wrong_answer

    def test_32(self):
        learn_page = "https://www.duolingo.com/learn"

        self.registration()
        self.learnPage.open_lesson()
        assert self.lessonPage.quit_lesson() == learn_page
