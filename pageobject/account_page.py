from basepage.base_page import BasePage


class AccountPage(BasePage):
    wrong_email = "aaaaaaaaaaaa"

    title_text_loc = "//*[@class=\"_3HPNX\"]"
    email_loc = "//*[@id=\"email\"]"
    error_loc = "//*[@class=\"_2Abn5 _3RRk-\"]"

    btn_save_change = "//*[@data-test=\"save-button\"]"

    def edit_profile(self):
        return self.get_url()

    def change_email(self):
        self.clear_input(self.email_loc)
        self.input(self.email_loc, self.wrong_email)

        self.click_button(self.btn_save_change)

        return self.element_text(self.error_loc)
