from basepage.base_page import BasePage


class CourseAboutPage(BasePage):
    title_text = "Раздел 1"
    learn_page = "https://www.duolingo.com/learn"

    title_text_loc = "//*[@class=\"_3kmfH\"]"

    btn_start_course = "//*[@data-test=\"course-start-button\"]"

    def add_language(self):
        self.click_button(self.btn_start_course)

        return self.element_text(self.title_text_loc)
