from basepage.base_page import BasePage
from selenium.webdriver.common.keys import Keys


class FriendsSearchPage(BasePage):
    wrong_user = "263213125"

    error_loc = "//*[@class=\"FrpXM\"]"
    search_loc = "//*[@class=\"_2q1-e\"]"

    def find_friends(self):
        self.input(self.search_loc, self.wrong_user)
        self.input(self.search_loc, Keys.ENTER)

        return self.element_text(self.error_loc)
