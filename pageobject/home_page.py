from basepage.base_page import BasePage


class HomePage(BasePage):

    url = "https://www.duolingo.com/"

    btn_have_account = "//*[@data-test=\"have-account\"]"

    def open_login(self):
        self.into_test(self.url)
        self.click_button(self.btn_have_account)
