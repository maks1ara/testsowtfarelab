from basepage.base_page import BasePage


class GuidePage(BasePage):
    title_text_loc = "//*[@class=\"_3_r63\"]"

    def check_guide(self):
        return self.element_text(self.title_text_loc)
