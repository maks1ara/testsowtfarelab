from basepage.base_page import BasePage


class AchievementsPage(BasePage):
    title_text_loc = "//*[@class=\"HBGOS\"]"

    def check_achievement(self):
        return self.element_text(self.title_text_loc)
