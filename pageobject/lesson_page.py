from basepage.base_page import BasePage


class LessonPage(BasePage):
    footer_color = "//*[@id=\"session/PlayerFooter\"]"

    btn_skip = "//*[@data-test=\"player-skip\"]"
    btn_quit = "//*[@data-test=\"quit-button\"]"

    def skip_question(self):
        self.click_button(self.btn_skip)

        return self.locate_element(self.footer_color).value_of_css_property('background-color')

    def quit_lesson(self):
        self.click_button(self.btn_quit)

        return self.get_url()
