from basepage.base_page import BasePage


class CoursesPage(BasePage):
    new_course_loc = "//*[@data-test=\"language-card language-de\"]"

    def open_course_about(self):
        self.click_button(self.new_course_loc)
