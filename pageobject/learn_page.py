from time import sleep
from basepage.base_page import BasePage


class LearnPage(BasePage):
    language = "Английский"
    notification_text_01 = "Выбор направления"
    notification_text_02 = "Мы рады новой встрече!"

    notification = "//*[@class=\"fs-unmask _270mD\"]"

    courses_loc = "//*[@data-test=\"courses-menu\"]"
    guide_loc = "//*[@data-test=\"guidebook-button-1\"]"
    open_lesson_01_loc = "/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/section[3]/div/div[3]/div/div/button"
    open_lesson_02_loc = "/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/section[1]/div/div[2]/div/div/button"

    blocked_lesson_01_loc = "/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/section[2]/div/div[6]/div/div/button"
    blocked_lesson_02_loc = "/html/body/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/section[1]/div/div[3]/div/div/button"
    notification_loc_01 = "//*[@class=\"DYCFd\"]"
    notification_loc_02 = "//*[@class=\"_171ia\"]"
    notification_loc_03 = "//*[@class=\"_2BqEp\"]"

    btn_begin_lesson = "//*[@href=\"/lesson\"]"
    btn_profile = "//*[@href=\"/profile/Maks1ara\"]"
    btn_add_course = "//*[@data-test=\"add-new-course\"]"
    btn_skip_notification_01 = "//*[@data-test=\"close-button\"]"
    btn_skip_notification_02 = "//*[@data-test=\"notification-drawer-no-thanks-button\"]"
    btn_skip_notification_03 = "//*[@data-test=\"notification-button\"]"

    def open_profile(self):
        self.click_button(self.btn_profile)

    def open_guide(self):
        self.click_button(self.guide_loc)

    def open_courses(self):
        self.hover(self.courses_loc)
        self.click_button(self.btn_add_course)

    def open_lesson(self):
        if self.element_text(self.courses_loc) == self.language:
            self.click_button(self.open_lesson_01_loc)
        else:
            self.click_button(self.open_lesson_02_loc)

        sleep(2)
        self.click_button(self.btn_begin_lesson)
        sleep(2)

    def open_block_lesson(self):
        if self.element_text(self.courses_loc) == self.language:
            self.click_button(self.blocked_lesson_01_loc)
        else:
            self.click_button(self.blocked_lesson_02_loc)

        return self.element_text(self.notification_loc_03)

    def skip_notification(self):
        if not self.notification:
            return

        if self.element_text(self.notification_loc_01) == self.notification_text_01:
            self.click_button(self.btn_skip_notification_01)
            try:
                if self.element_text(self.notification_loc_02) == self.notification_text_02:
                    self.click_button(self.btn_skip_notification_02)
            except():
                return
