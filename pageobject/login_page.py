from basepage.base_page import BasePage


class LoginPage(BasePage):

    email = "maks.tyunev@mail.ru"
    password = "passworddualingo"

    email_loc = "//*[@data-test=\"email-input\"]"
    password_loc = "//*[@data-test=\"password-input\"]"
    btn_register = "//*[@data-test=\"register-button\"]"

    def open_learn(self):
        self.input(self.email_loc, self.email)
        self.input(self.password_loc, self.password)

        self.click_button(self.btn_register)
