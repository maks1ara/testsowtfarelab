from basepage.base_page import BasePage


class ProfilePage(BasePage):

    btn_edit = "//*[@href=\"/settings/account\"]"
    btn_show_ach = "//*[@href=\"/profile/Maks1ara/achievements\"]"
    btn_find_friends = "//*[@data-test=\"social-panel-find-friends\"]"

    def open_account(self):
        self.click_button(self.btn_edit)

    def open_achievements(self):
        self.click_button(self.btn_show_ach)

    def open_friends_search(self):
        self.click_button(self.btn_find_friends)