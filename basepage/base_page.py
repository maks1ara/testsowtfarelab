from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


def wait_of_element_located(xpath, driver):
    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located(
                (By.XPATH, xpath)
            )
        )
        return element
    except TimeoutException:
        return None


class BasePage:
    def __init__(self, driver):
        self.driver = driver

    # Open page
    def into_test(self, url):
        self.driver.get(url)

    # Positioning element
    def locate_element(self, args):
        return wait_of_element_located(args, self.driver)

    # Get the current text
    def element_text(self, args):
        if self.locate_element(args) is not None:
            return self.locate_element(args).text

    # Get the current url
    def get_url(self):
        return str(self.driver.current_url)

    # Hover to element
    def hover(self, args):
        if self.locate_element(args) is not None:
            ActionChains(self.driver).move_to_element(self.locate_element(args)).perform()

    # Clear input
    def clear_input(self, args):
        if self.locate_element(args) is not None:
            self.locate_element(args).send_keys(Keys.CONTROL + "a")
            self.locate_element(args).send_keys(Keys.BACKSPACE)

    # Input value
    def input(self, args, text):
        if self.locate_element(args) is not None:
            self.locate_element(args).send_keys(text)

    # Click the button
    def click_button(self, args):
        if self.locate_element(args) is not None:
            self.locate_element(args).click()
