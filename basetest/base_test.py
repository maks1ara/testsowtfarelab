from unittest import TestCase
import pytest
from selenium import webdriver
from selenium.webdriver.edge.service import Service


@pytest.mark.usefixtures('driver_init')
class BaseTest(TestCase):

    @pytest.fixture(autouse=True)
    def driver_init(self, request):
        PATH = Service("msedgedriver.exe")

        options = webdriver.EdgeOptions()
        options.add_argument("-inprivate")
        options.add_argument("start-maximized")
        options.add_experimental_option("excludeSwitches", ["enable-automation"])

        driver = webdriver.Edge(service=PATH, options=options)

        request.cls.driver = driver

        yield
        driver.close()
